import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { FaShoppingCart } from 'react-icons/fa'
import { useDispatch, useSelector } from 'react-redux'
import Button from './elements/Button'
import routes from '../constants/routes.json'
import { openCart } from '../state/actions'
import "./Header.css"

const Header = () => {
  const cart = useSelector((state) => state.cart)
  const dispatch = useDispatch()

  const sumQuantity = () => {
    return cart.reduce((quantity, cartItem) => quantity + cartItem.quantity, 0)
  }

  return (
    <header className="HeaderWrapper">
      <div className="Container">
        <Link to={routes.HOME}>
          <h1 className="Logo">FakeStore</h1>
        </Link>
        <nav className="Navbar">
          <Link className="NavbarLink" to={routes.HOME}>Home</Link>
          <Link className="NavbarLink" to={routes.PRODUCTS}>Products</Link>
          <div id='cartIcon' className="ButtonContainer" onClick={() => dispatch(openCart())}>
            <Button content={<FaShoppingCart />} shape="round" />
            {sumQuantity() > 0 ? <div className="Quantity">{sumQuantity()}</div> : ''}
          </div>
        </nav>
      </div>
    </header>
  )
}

export default Header
