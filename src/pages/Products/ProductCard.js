import React from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "../../components/elements/Button";
import { addToCart } from "../../state/actions/cart";
import "./ProductCard.css";

const ProductCard = ({ id, title, price, image, category }) => {
  const product = { id, title, price, image, category };
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart)

  const getQuantity = (id) => cart.find(p => p.id === id) ? `(${cart.find(p => p.id === id).quantity})` : "";

  return (
    <div productCardId="cy-productId" className="ProductCardWrapper">
      <div className="ImageContainer">
        <img className="Image" src={image} alt={title} />
      </div>
      <div className="Details">
        <div className="Info">
          <div className="Title">{title}</div>
          <div>${price.toFixed(2)}</div>
        </div>
        <Button
          onClick={() => dispatch(addToCart(product))}
          content={`Add to card ${getQuantity(id)}`}
          size="wide"
          color="dark"
          animation="color"
        />
      </div>
    </div>
  );
};

export default ProductCard;