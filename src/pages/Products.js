import React from "react";
import { v4 as uuidv4 } from "uuid";
import { useDispatch, useSelector } from "react-redux";
import ProductCard from "./Products/ProductCard";
import { setProducts } from "../state/actions/products";
import { useQuery } from "react-query";
import "./products.css";

const Products = () => {
  const products = useSelector((state) => state.products);
  const dispatch = useDispatch();

  const fetchProducts = async () => {
    const response = await fetch("https://fakestoreapi.com/products");
    let data = await response.json();
    return data;
  };

  const { isLoading, isError } = useQuery("products", fetchProducts, {
    onSuccess: (data) =>
      dispatch(
        setProducts(
          data.filter((product) => product.category === `electronics`)
        )
      ),
  });

  if (isLoading) return <div>Loading...</div>;
  if (isError) return <div>Error fetching data</div>;

  const productCards = products.map((product) => (
    <ProductCard
      key={uuidv4()}
      id={product.id}
      title={product.title}
      price={product.price}
      image={product.image}
    />
  ));

  return (
      <div className="productWrapper">
        {products.map((product) => (
          <ProductCard
            key={uuidv4()}
            id={product.id}
            title={product.title}
            price={product.price}
            image={product.image}
          />
        ))}
      </div>
  );
};

export default Products;
