import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Header from './components/Header'

import Main from "./components/Main";
import Cart from "./components/Cart";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Header />
        <Main />
      </Router>
      <Cart />
    </QueryClientProvider>
  );
};

export default App;
