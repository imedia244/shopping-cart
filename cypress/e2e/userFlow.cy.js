describe('template spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000/shopping-cart/products')
    
    cy.get('[productcardid="cy-productId"]').should('exist'); // -- check if the product card exist 
    cy.get('[productcardid="cy-productId"]').first().as('firstProduct'); // -- select the first product
    cy.get('@firstProduct').find('#addtocard').click(); 
    
    // -- check if the cartIcon has the Quantity tag equal to 1
    cy.get('#cartIcon').should('exist');
    cy.get('#cartIcon').find('.Quantity').should('exist');
    cy.get('#cartIcon').find('.Quantity').should('contain', '1');
  })

})