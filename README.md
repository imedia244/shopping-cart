# Shopping cart

A simple fake store React app.

## Getting started

```
cd shopping-cart
npm install
npm start
```

## Built with

- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [React Router](https://reactrouter.com/)
- [Fake Store API](https://fakestoreapi.com/)
- [React Query](https://tanstack.com/query/v3/)
